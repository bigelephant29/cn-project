#include<vector>
#include<algorithm>
#include<QtCore>
#include<QObject>
#include<QDebug>
#include<QtNetwork>
#include<QJsonDocument>
#include<QJsonObject>
#include<QThread>

#include<hiredis/hiredis.h>

class MsgChannel : public QThread {
    Q_OBJECT
    private:
	qintptr sockdesc;
	QTcpSocket *conn;
	redisContext *rs;

	QString selfname;
	bool login;

    public:
	MsgChannel(qintptr _sockdesc) : sockdesc(_sockdesc), login(false) {}
	void run() {
	    qDebug() << "start thread";
	    conn = new QTcpSocket();
	    connect(conn, SIGNAL(disconnected()), this, SLOT(slot_disconnect()));
	    conn->setSocketDescriptor(sockdesc);

	    rs = redisConnect("localhost", 6379);

	    QJsonDocument doc;
	    redisReply *rsret;
	    int intret;

	    while(readJSON(doc)) {
		qDebug() << doc.toJson();

		QJsonObject obj, ret;

		obj = doc.object();
		QString type = obj["type"].toString();

		if(type == "register") {
		    QString account = obj["account"].toString();
		    QString password = obj["password"].toString();

		    rsret = (redisReply*)redisCommand(rs, "SISMEMBER USERSET %s", account.toUtf8().constData());
		    intret = rsret->integer;
		    freeReplyObject(rsret);
		    if(intret != 0) {
			ret["type"] = "reply";
			ret["status"] = "exist";
			sendJSON(ret);

		    } else {
			QString accountkey = "ACCOUNT@" + account;
			redisCommand(rs, "SADD USERSET %s", account.toUtf8().constData());
			redisCommand(rs, "HSET %s password %s",
			    accountkey.toUtf8().constData(), password.toUtf8().constData());
			ret["type"] = "reply";
			ret["status"] = "ok";
			sendJSON(ret);
		    }
		    continue;

		} else if(type == "login") {
		    QString account = obj["account"].toString();
		    QString password = obj["password"].toString();

		    rsret = (redisReply*)redisCommand(rs, "SISMEMBER USERSET %s", account.toUtf8().constData());
		    intret = rsret->integer;
		    freeReplyObject(rsret);
		    if(intret == 0) {
			ret["type"] = "reply";
			ret["status"] = "error";
			sendJSON(ret);

		    } else {
			QString accountkey = "ACCOUNT@" + account;
			rsret = (redisReply*)redisCommand(rs, "HGET %s password",
			    accountkey.toUtf8().constData());
			QString retpw = rsret->str;
			freeReplyObject(rsret);

			ret["type"] = "reply";
			if(retpw == password) {
			    selfname = account;
			    login = true;
			    redisCommand(rs, "SADD ONLINESET %s", selfname.toUtf8().constData());
			    ret["status"] = "ok";
			} else {
			    ret["status"] = "error";
			}
			sendJSON(ret);
		    }
		    continue;
		} else if(type == "upload_file") {
		    QString fileid = obj["fileid"].toString();

		    rsret = (redisReply*)redisCommand(rs, "SISMEMBER FILESET %s", fileid.toUtf8().constData());
		    intret = rsret->integer;
		    freeReplyObject(rsret);
		    if(intret == 1) {
			save_file(fileid);
		    }
		    conn->disconnectFromHost();
		    continue;
		} else if(type == "download_file") {
		    QString fileid = obj["fileid"].toString();

		    rsret = (redisReply*)redisCommand(rs, "SISMEMBER FILESET %s", fileid.toUtf8().constData());
		    intret = rsret->integer;
		    freeReplyObject(rsret);
		    if(intret == 1) {
			read_file(fileid);
		    }
		    conn->disconnectFromHost();
		    continue;
		} else if(login == false) {
		    ret["type"] = "reply";
		    ret["status"] = "error";
		    sendJSON(ret);
		    continue;
		}

		if(type == "get_userlist") {
		    unsigned long i;
		    QVector<QString> userlist;
		    QVector<QString>::iterator userlist_it;
		    QSet<QString> onlineset;
		    QJsonArray jsonlist;

		    rsret = (redisReply*)redisCommand(rs, "SMEMBERS USERSET");
		    for(i = 0;i < rsret->elements;i++) {
			QString name = ((redisReply*)rsret->element[i])->str;
			userlist.push_back(name);
		    }
		    freeReplyObject(rsret);
		    rsret = (redisReply*)redisCommand(rs, "SMEMBERS ONLINESET");
		    for(i = 0;i < rsret->elements;i++) {
			QString name = ((redisReply*)rsret->element[i])->str;
			onlineset.insert(name);
		    }
		    freeReplyObject(rsret);

		    for(userlist_it = userlist.begin(); userlist_it != userlist.end(); userlist_it++) {
			QJsonObject item;

			item["account"] = *userlist_it;
			if(onlineset.find(*userlist_it) != onlineset.constEnd()) {
			    item["status"] = "online";
			} else {
			    item["status"] = "offline";
			}

			jsonlist.append(item);
		    }

		    ret["type"] = "reply";
		    ret["list"] = jsonlist;
		    sendJSON(ret);
		} else if(type == "get_roomlist") {
		    unsigned long i;
		    QJsonArray jsonlist;

		    rsret = (redisReply*)redisCommand(rs, "SMEMBERS ROOMSET@%s", selfname.toUtf8().constData());
		    for(i = 0;i < rsret->elements;i++) {
			QString room_name = ((redisReply*)rsret->element[i])->str;
			jsonlist.append(room_name);
		    }
		    freeReplyObject(rsret);

		    ret["type"] = "reply";
		    ret["list"] = jsonlist;
		    sendJSON(ret);
		} else if(type == "join_room") {
		    QString target_name = obj["account"].toString();
		    QString target_room = obj["room"].toString();

		    rsret = (redisReply*)redisCommand(rs, "SADD ROOMSET@%s %s",
			target_name.toUtf8().constData(), target_room.toUtf8().constData());
		    freeReplyObject(rsret);
		    
		    ret["type"] = "reply";
		    ret["status"] = "ok";
		    sendJSON(ret);
		} else if(type == "leave_room") {
		    QString target_room = obj["room"].toString();

		    rsret = (redisReply*)redisCommand(rs, "SREM ROOMSET@%s %s",
			selfname.toUtf8().constData(), target_room.toUtf8().constData());
		    freeReplyObject(rsret);
		    
		    ret["type"] = "reply";
		    ret["status"] = "ok";
		    sendJSON(ret);
		} else if(type == "get_message") {
		    unsigned int i;
		    QString target_room = obj["room"].toString();
		    unsigned long timestamp = obj["timestamp"].toInt();
		    QJsonDocument msgdoc;
		    QJsonObject msgobj;
		    QJsonArray jsonlist;

		    rsret = (redisReply*)redisCommand(rs, "ZRANGEBYSCORE ROOM@%s %d +inf",
			target_room.toUtf8().constData(), timestamp);
		    for(i = 0;i < rsret->elements;i++) {
			const char *str = ((redisReply*)rsret->element[i])->str;
			msgdoc = QJsonDocument::fromJson(str);
			msgobj = msgdoc.object();
			jsonlist.append(msgobj);
		    }
		    freeReplyObject(rsret);
		    
		    ret["type"] = "reply";
		    ret["list"] = jsonlist;
		    sendJSON(ret);
		} else if(type == "send_message") {
		    QString target_room = obj["room"].toString();
		    QString text = obj["text"].toString();
		    unsigned long timestamp = QDateTime::currentMSecsSinceEpoch();
		    QJsonDocument msgdoc;
		    QJsonObject msgobj;

		    msgobj["timestamp"] = (qint32)timestamp;
		    msgobj["sender"] = selfname;
		    msgobj["text"] = text;
		    msgobj["fileid"] = "";
		    msgdoc.setObject(msgobj);

		    rsret = (redisReply*)redisCommand(rs, "ZADD ROOM@%s %d %s",
			target_room.toUtf8().constData(), timestamp, msgdoc.toJson().constData());
		    freeReplyObject(rsret);

		    ret["type"] = "reply";
		    ret["status"] = "ok";
		    sendJSON(ret);
		} else if(type == "get_file") {
		} else if(type == "create_file") {
		    QString fileid = QUuid::createUuid().toString();

		    rsret = (redisReply*)redisCommand(rs, "SADD FILESET %s", fileid.toUtf8().constData());
		    freeReplyObject(rsret);

		    ret["type"] = "reply";
		    ret["fileid"] = fileid;
		    sendJSON(ret);
		}
	    }
	}

    private slots:
	void slot_disconnect() {
	    redisCommand(rs, "SREM ONLINESET %s", selfname.toUtf8().constData());

	    delete conn;
	    conn = NULL;
	    redisFree(rs);
	    quit();
	}

    private:
	bool read_buffer(QByteArray &bytebuf) {
	    QDataStream datastream(conn);
	    datastream.setVersion(QDataStream::Qt_5_0);
	    quint64 remain;
	    quint64 length;

	    if(conn->bytesAvailable() == 0 && !conn->waitForReadyRead(-1)) {
		return false;
	    }
	    remain = conn->bytesAvailable();
	    Q_ASSERT(remain >= 8);
	    datastream >> length;
	    remain -= 8;

	    bytebuf.clear();
	    while(length > 0) {
		if(remain > 0) {
		    char *buf = new char[std::min(length, remain)];
		    int len = datastream.readRawData(buf, std::min(length, remain));
		    if(len == -1) {
			delete buf;
			return false;
		    } else {
			bytebuf.append(buf, std::min(length, remain));
			length -= std::min(length, remain);
			delete buf;
		    }
		}
		if(length == 0) {
		    break;
		}
		if(!conn->waitForReadyRead(-1)) {
		    return false;
		}
		remain = conn->bytesAvailable();
	    }

	    return true;
	}
	bool read_buffer(QDataStream &outstream) {
	    QDataStream datastream(conn);
	    datastream.setVersion(QDataStream::Qt_5_0);
	    quint64 remain;
	    quint64 length;

	    if(conn->bytesAvailable() == 0 && !conn->waitForReadyRead(-1)) {
		return false;
	    }
	    remain = conn->bytesAvailable();
	    Q_ASSERT(remain >= 8);
	    datastream >> length;
	    remain -= 8;

	    while(length > 0) {
		if(remain > 0) {
		    char *buf = new char[std::min(length, remain)];
		    int len = datastream.readRawData(buf, std::min(length, remain));
		    if(len == -1) {
			delete buf;
			return false;
		    } else {
			outstream.writeRawData(buf, len);
			length -= std::min(length, remain);
			delete buf;
		    }
		}
		if(length == 0) {
		    break;
		}
		if(!conn->waitForReadyRead(-1)) {
		    return false;
		}
		remain = conn->bytesAvailable();
	    }

	    return true;
	}
	bool save_file(QString fileid) {
	    QString filename = "file/" + \
		QCryptographicHash::hash(fileid.toUtf8(), QCryptographicHash::Sha256).toHex();
	    QFile file(filename);

	    if(!file.open(QIODevice::WriteOnly)) {
		return false;
	    }

	    QDataStream datastream(&file);
	    read_buffer(datastream);
	    file.flush();
	    file.close();

	    return true;
	}
	bool read_file(QString fileid) {
	    QString filename = "file/" + \
		QCryptographicHash::hash(fileid.toUtf8(), QCryptographicHash::Sha256).toHex();
	    QFile file(filename);

	    if(!file.open(QIODevice::ReadOnly)) {
		return false;
	    }

	    QDataStream netstream(conn);
	    char buf[65536];
	    
	    netstream << (quint64)file.size();

	    while(true) {
		qint64 ret = file.read(buf, sizeof(buf));
		if(ret <= 0) {
		    break;
		}
		netstream.writeRawData(buf, ret);
		conn->waitForBytesWritten(-1);
	    }
	    file.close();

	    return conn->waitForBytesWritten(-1);
	}
	bool sendJSON(QJsonObject &obj) {
	    QJsonDocument doc;
	    doc.setObject(obj);
	    return sendJSON(doc);
	}
	bool sendJSON(QJsonDocument &doc) {
	    QByteArray jsonbuf;
	    QByteArray data;
	    QDataStream datastream(&data, QIODevice::ReadWrite);
	    datastream.setVersion(QDataStream::Qt_5_0);

	    jsonbuf = doc.toBinaryData();
	    datastream << (quint64)jsonbuf.size();
	    datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
	    conn->write(data);

	    return conn->waitForBytesWritten(-1);
	}
	bool readJSON(QJsonDocument &doc) {
	    QByteArray jsonbuf;
	    if(!read_buffer(jsonbuf)) {
		return false;
	    }
	    doc = QJsonDocument::fromBinaryData(jsonbuf);
	    return true;
	}


};
class Server : public QTcpServer {
    Q_OBJECT
    public:
	Server(QObject *parent=0) : QTcpServer(parent) {}

    protected:
	void incomingConnection(qintptr sockdesc) {
	    MsgChannel *ch = new MsgChannel(sockdesc);
	    ch->start();
	}
};

class User{
    public:
	QString account;
	QString status;
	User(QString _account, QString _status) :
	    account(_account), status(_status) {}
};
class Message{
    public:
	QString sender;
	QString text;
	QString fileid;
	Message(QString _sender, QString _text, QString _fileid) :
	    sender(_sender), text(_text), fileid(_fileid) {}
};
class Room{
    public:
	QString name;
	Room(QString _name) : name(_name) {}
};

class UploadFile : public QThread {
    Q_OBJECT
    private:
	QTcpSocket *conn;
	QString filepath;
	QString fileid;

    signals:
	void uploaded(QString fileid);

    public:
	UploadFile(QString _filepath, QString _fileid) : filepath(_filepath), fileid(_fileid) {}
	void run() {
	    conn = new QTcpSocket();
	    conn->connectToHost("127.0.0.1", 10000);
	    conn->waitForConnected(-1);

	    QJsonObject obj;
	    obj["type"] = "upload_file";
	    obj["fileid"] = fileid;
	    sendJSON(obj);

	    upload_file(filepath);

	    emit uploaded(fileid);
	    quit();
	}
	bool sendJSON(QJsonObject &obj) {
	    QJsonDocument doc;
	    doc.setObject(obj);

	    QByteArray jsonbuf;
	    QByteArray data;
	    QDataStream datastream(&data, QIODevice::ReadWrite);
	    datastream.setVersion(QDataStream::Qt_5_0);

	    jsonbuf = doc.toBinaryData();
	    datastream << (quint64)jsonbuf.size();
	    datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
	    conn->write(data);

	    return conn->waitForBytesWritten(-1);
	}
	bool upload_file(QString path) {
	    QFile file(path);

	    if(!file.open(QIODevice::ReadOnly)) {
		return false;
	    }

	    QDataStream netstream(conn);
	    char buf[65536];
	    
	    netstream << (quint64)file.size();

	    while(true) {
		qint64 ret = file.read(buf, sizeof(buf));
		if(ret <= 0) {
		    break;
		}
		netstream.writeRawData(buf, ret);
		conn->waitForBytesWritten(-1);
	    }
	    file.close();

	    conn->waitForBytesWritten(-1);
	    conn->waitForDisconnected();
	    return true;
	}
};
class DownloadFile : public QThread {
    Q_OBJECT
    private:
	QTcpSocket *conn;
	QString filepath;
	QString fileid;

    signals:
	void downloaded(QString fileid);

    public:
	DownloadFile(QString _filepath, QString _fileid) : filepath(_filepath), fileid(_fileid) {}
	void run() {
	    conn = new QTcpSocket();
	    conn->connectToHost("127.0.0.1", 10000);
	    conn->waitForConnected(-1);

	    QJsonObject obj;
	    obj["type"] = "download_file";
	    obj["fileid"] = fileid;
	    sendJSON(obj);

	    download_file(filepath);

	    emit downloaded(fileid);
	    quit();
	}
	bool sendJSON(QJsonObject &obj) {
	    QJsonDocument doc;
	    doc.setObject(obj);

	    QByteArray jsonbuf;
	    QByteArray data;
	    QDataStream datastream(&data, QIODevice::ReadWrite);
	    datastream.setVersion(QDataStream::Qt_5_0);

	    jsonbuf = doc.toBinaryData();
	    datastream << (quint64)jsonbuf.size();
	    datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
	    conn->write(data);

	    return conn->waitForBytesWritten(-1);
	}
	bool read_buffer(QDataStream &outstream) {
	    QDataStream datastream(conn);
	    datastream.setVersion(QDataStream::Qt_5_0);
	    quint64 remain;
	    quint64 length;

	    if(conn->bytesAvailable() == 0 && !conn->waitForReadyRead(-1)) {
		return false;
	    }
	    remain = conn->bytesAvailable();
	    Q_ASSERT(remain >= 8);
	    datastream >> length;
	    remain -= 8;

	    qDebug() << length;

	    while(length > 0) {
		if(remain > 0) {
		    char *buf = new char[std::min(length, remain)];
		    int len = datastream.readRawData(buf, std::min(length, remain));
		    if(len == -1) {
			delete buf;
			return false;
		    } else {
			outstream.writeRawData(buf, len);
			length -= std::min(length, remain);
			delete buf;
		    }
		}
		if(length == 0) {
		    break;
		}
		if(!conn->waitForReadyRead(-1)) {
		    return false;
		}
		remain = conn->bytesAvailable();
	    }

	    return true;
	}
	bool download_file(QString path) {
	    QFile file(path);

	    if(!file.open(QIODevice::WriteOnly)) {
		return false;
	    }

	    QDataStream datastream(&file);
	    read_buffer(datastream);
	    file.flush();
	    file.close();

	    conn->waitForDisconnected();
	    return true;
	}
};

class TestClient : public QThread {
    Q_OBJECT
    private:
	QTcpSocket *conn;

    private slots:
	void slot_uploaded(QString fileid) {
	    qDebug() << fileid;
	}
	void slot_downloaded(QString fileid) {
	    qDebug() << fileid;
	}

    public:
	bool reg(QString account, QString password) {
	    QJsonObject obj;
	    QJsonDocument doc;
	    obj["type"] = "register";
	    obj["account"] = account;
	    obj["password"] = password;
	    sendJSON(obj);
	    readJSON(doc);
	    return doc.object()["status"] == "ok";
	}
	bool login(QString account, QString password) {
	    QJsonObject obj;
	    QJsonDocument doc;
	    obj["type"] = "login";
	    obj["account"] = account;
	    obj["password"] = password;
	    sendJSON(obj);
	    readJSON(doc);
	    return doc.object()["status"] == "ok";
	}
	bool get_userlist(std::vector<User> &userlist) {
	    int i;
	    QJsonObject obj;
	    QJsonDocument doc;
	    QJsonArray jsonlist;

	    obj["type"] = "get_userlist";
	    sendJSON(obj);
	    readJSON(doc);

	    jsonlist = doc.object()["list"].toArray();
	    for(i = 0;i < jsonlist.size();i++) {
		userlist.push_back(User(jsonlist[i].toObject()["account"].toString(),
		    jsonlist[i].toObject()["status"].toString()));
	    }
	    return true;
	}
	bool join_room(QString room_name, QString account_name) {
	    QJsonObject obj;
	    QJsonDocument doc;
	    obj["type"] = "join_room";
	    obj["room"] = room_name;
	    obj["account"] = account_name;
	    sendJSON(obj);
	    readJSON(doc);
	    return doc.object()["status"] == "ok";
	}
	bool leave_room(QString room_name) {
	    QJsonObject obj;
	    QJsonDocument doc;
	    obj["type"] = "leave_room";
	    obj["room"] = room_name;
	    sendJSON(obj);
	    readJSON(doc);
	    return doc.object()["status"] == "ok";
	}
	bool get_roomlist(std::vector<Room> &roomlist) {
	    int i;
	    QJsonObject obj;
	    QJsonDocument doc;
	    QJsonArray jsonlist;

	    obj["type"] = "get_roomlist";
	    sendJSON(obj);
	    readJSON(doc);

	    jsonlist = doc.object()["list"].toArray();
	    for(i = 0;i < jsonlist.size();i++) {
		roomlist.push_back(Room(jsonlist[i].toString()));
	    }
	    return true;
	}
	bool send_message(QString room_name, QString text) {
	    QJsonObject obj;
	    QJsonDocument doc;
	    obj["type"] = "send_message";
	    obj["room"] = room_name;
	    obj["text"] = text;
	    sendJSON(obj);
	    readJSON(doc);
	    return doc.object()["status"] == "ok";
	}
	qint32 get_message(QString room, qint32 timestamp, std::vector<Message> &msglist) {
	    int i;
	    QJsonObject obj;
	    QJsonDocument doc;
	    QJsonArray jsonlist;
	    
	    obj["type"] = "get_message";
	    obj["room"] = room;
	    obj["timestamp"] = (qint32)timestamp;
	    sendJSON(obj);
	    readJSON(doc);

	    jsonlist = doc.object()["list"].toArray();
	    for(i = 0;i < jsonlist.size();i++) {
		QJsonObject msgobj = jsonlist[i].toObject();
		msglist.push_back(Message(
		    msgobj["sender"].toString(), msgobj["text"].toString(), msgobj["fileid"].toString()));
		timestamp = std::max(timestamp, msgobj["timestamp"].toInt());
	    }

	    return timestamp;
	}
	QString send_file(QString filepath) {
	    QJsonObject obj;
	    QJsonDocument doc;
	    
	    obj["type"] = "create_file";
	    sendJSON(obj);
	    readJSON(doc);
	    QString fileid = doc.object()["fileid"].toString();

	    UploadFile *filethread = new UploadFile(filepath, fileid);
	    filethread->start();
	    connect(filethread, SIGNAL(uploaded(QString)), this, SLOT(slot_uploaded(QString)));
	    connect(filethread, SIGNAL(finished()), filethread, SLOT(deleteLater()));

	    return fileid;
	}
	bool download_file(QString fileid, QString filepath) {
	    DownloadFile *filethread = new DownloadFile(filepath, fileid);
	    filethread->start();
	    connect(filethread, SIGNAL(downloaded(QString)), this, SLOT(slot_downloaded(QString)));
	    connect(filethread, SIGNAL(finished()), filethread, SLOT(deleteLater()));

	    return true;
	}

	void run() {
	    unsigned int i;

	    conn = new QTcpSocket();
	    conn->connectToHost("127.0.0.1", 10000);
	    conn->waitForConnected(-1);

	    qDebug() << reg("who", "1234");
	    qDebug() << login("who", "1234");

	    std::vector<User> userlist;
	    get_userlist(userlist);
	    for(i = 0;i < userlist.size();i++) {
		qDebug() << userlist[i].account;
		qDebug() << userlist[i].status;
	    }

	    qDebug() << join_room("ThisIsRoom", "who");
	    std::vector<Room> roomlist;
	    get_roomlist(roomlist);
	    for(i = 0;i < roomlist.size();i++) {
		qDebug() << roomlist[i].name;
	    }
	    
	    qDebug() << send_message("ThisIsRoom", "Hello");
	    std::vector<Message> msglist;
	    qDebug() << get_message("ThisIsRoom", 1669418246, msglist);
	    for(i = 0;i < msglist.size();i++) {
		qDebug() << msglist[i].sender;
		qDebug() << msglist[i].text;
	    }

	    qDebug() << leave_room("ThisIsRoom");

	    //send_file("/boot/vmlinuz-3.16.0-4-amd64");
	    download_file("{d9425ce2-e57b-4aa4-9174-40bd0dd9d06c}", "/tmp/x");

	    conn->disconnectFromHost();
	    quit();
	}

    private:
	bool sendJSON(QJsonObject &obj) {
	    QJsonDocument doc;
	    doc.setObject(obj);
	    return sendJSON(doc);
	}
	bool sendJSON(QJsonDocument &doc) {
	    QByteArray jsonbuf;
	    QByteArray data;
	    QDataStream datastream(&data, QIODevice::ReadWrite);
	    datastream.setVersion(QDataStream::Qt_5_0);

	    jsonbuf = doc.toBinaryData();
	    datastream << (quint64)jsonbuf.size();
	    datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
	    conn->write(data);

	    return conn->waitForBytesWritten(-1);
	}
	bool readJSON(QJsonDocument &doc) {
	    QDataStream datastream(conn);
	    datastream.setVersion(QDataStream::Qt_5_0);
	    QByteArray jsonbuf;
	    quint64 remain;
	    quint64 length;

	    if(conn->bytesAvailable() == 0 && !conn->waitForReadyRead(-1)) {
		return false;
	    }
	    remain = conn->bytesAvailable();
	    Q_ASSERT(remain >= 8);
	    datastream >> length;
	    remain -= 8;

	    while(length > 0) {
		if(remain > 0) {
		    char *buf = new char[std::min(length, remain)];
		    datastream.readRawData(buf, std::min(length, remain));
		    jsonbuf.append(buf, std::min(length, remain));
		    length -= std::min(length, remain);
		    delete buf;
		}
		if(length == 0) {
		    break;
		}
		if(!conn->waitForReadyRead(-1)) {
		    return false;
		}
		remain = conn->bytesAvailable();
	    }

	    doc = QJsonDocument::fromBinaryData(jsonbuf);
	    return true;
	}
};

#include"server.moc"
int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    Server serv;
    serv.listen(QHostAddress::Any, 10000);

    TestClient test;
    test.start();

    return app.exec();
}
