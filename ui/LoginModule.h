#ifndef LOGINMODULE
#define LOGINMODULE

#include <QString>
#include <QApplication>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QObject>

class LoginModule : public QObject{
    Q_OBJECT
private:
    QVBoxLayout *loginLayout;
    QLineEdit *idInput;
    QLineEdit *pwdInput;
    QPushButton *loginBtn;
    QPushButton *regBtn;
    QPushButton *cancelBtn;
public slots:
    void checkLoginOnClick();
public:
    QVBoxLayout *setupLoginPage();        /* Function used to set up the log-in page */
signals:
    void loginSucceed();
    void loginFail();
    void loginRequest(QString, QString);
};

#endif
