#include "WindowModule.h"
#include <QLabel>
#include <QTimer>

void WindowModule :: slot_connect() {
    /* connect to server */
    client = new Client(this, ipInput->text(), portInput->text());
    client -> start();

    QTimer *updateTimer = new QTimer(NULL);
    QObject::connect(updateTimer, SIGNAL(timeout()), client, SLOT(updateStatus()));
    updateTimer->start(1000);
}

void WindowModule :: mainPage(){
    connLayout = new QFormLayout();
    ipInput = new QLineEdit();
    ipInput->setText("140.112.31.101");
    connLayout->addRow("IP:", ipInput);
    portInput = new QLineEdit();
    portInput->setText("10000");
    connLayout->addRow("Port:", portInput);

    QPushButton *connBtn = new QPushButton("Connect");
    connLayout->addRow(connBtn);

    this -> setLayout(connLayout);
    this -> show();

    QObject::connect(connBtn, SIGNAL(clicked()), this, SLOT(slot_connect()));
}

void WindowModule :: setupMainPage(){
    QObject::connect(this, SIGNAL(loginRequest(QString,QString)), client, SLOT(loginRequest(QString,QString)));
    QObject::connect(this, SIGNAL(loginSucceed()), this, SLOT(switchToLobbyPage()));

    login.setParent(this);
    loginLayout = login.setupLoginPage();
    deleteQLayout(connLayout);
    this -> hide();
    this -> setLayout(loginLayout);
    this -> adjustSize();
    this -> show();
}

void WindowModule :: setupFailPage(){
    QLabel *label = new QLabel(" Fail to connect to server Q_Q");
    label->setWindowTitle("HI N D is carry");
    label->resize(200, 50);
    label->show();
}

void WindowModule :: deleteQLayout (QLayout *layout){
    QLayoutItem *item;
    while( layout -> count() > 0 ){
        item = layout -> takeAt(0);
        if(item -> layout()){
            deleteQLayout(item -> layout());
        }
        else if(item -> widget()) delete item -> widget();
    }
    delete layout;
}

void WindowModule :: switchToMainPage (){
    this -> hide();
    deleteQLayout(registerLayout);
    login.setParent(this);
    loginLayout = login.setupLoginPage();
    this -> setLayout(loginLayout);
    this -> adjustSize();
    this -> show();
}

void WindowModule :: switchToRegisterPage (){
    QObject::connect(this, SIGNAL(registerRequest(QString, QString)), client, SLOT(registerRequest(QString, QString)), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(registerSucceed()), this, SLOT(switchToMainPage()), Qt::UniqueConnection);

    this -> hide();
    deleteQLayout(loginLayout);
    regpage.setParent(this);
    registerLayout = regpage.setupRegisterPage();
    this -> setLayout(registerLayout);
    this -> adjustSize();
    this -> show();
}

void WindowModule :: switchToLobbyPage (){
    QObject::connect(this, SIGNAL(getUserListRequest()), client, SLOT(getUserListRequest()));
    QObject::connect(client, SIGNAL(updateUserList(QStringList, QStringList)), &lobby, SLOT(updateUserList(QStringList, QStringList)));
    QObject::connect(this, SIGNAL(createRoomRequest(QString)), client, SLOT(createRoomRequest(QString)));
    QObject::connect(client, SIGNAL(addRoom(QString)), &lobby, SLOT(addRoom(QString)));
    QObject::connect(this, SIGNAL(leaveRoomRequest(QString)), client, SLOT(leaveRoomRequest(QString)));
    QObject::connect(client, SIGNAL(removeRoom(QString)), &lobby, SLOT(removeRoom(QString)));
    QObject::connect(client, SIGNAL(updateMessage(QString,QStringList,quint32,QStringList)), &lobby, SIGNAL(updateMessage(QString, QStringList,quint32,QStringList)));
    QObject::connect(this, SIGNAL(sendText(QString,QString)), client, SLOT(sendText(QString, QString)));
    QObject::connect(client, SIGNAL(updateMyMessage(QString)), &lobby, SIGNAL(updateMyMessage(QString)));
    QObject::connect(this, SIGNAL(sendFile(QString)), client, SLOT(sendFile(QString)));
    QObject::connect(client, SIGNAL(fileTransferMessage(QString)), &lobby, SIGNAL(fileTransferMessage(QString)));
    QObject::connect(this, SIGNAL(inviteUser(QString,QString)), client, SLOT(inviteUser(QString,QString)));
    QObject::connect(this, SIGNAL(downloadFile(QString,QString)), client, SLOT(downloadFile(QString,QString)));
    QObject::connect(this, SIGNAL(setRoomName(QString)), client, SLOT(setRoomName(QString)));
    QObject::connect(client, SIGNAL(updateRoomList(QStringList)), &lobby, SLOT(updateRoomList(QStringList)));

    this -> hide();
    deleteQLayout(loginLayout); 
    lobby.setParent(this);
    lobbyLayout = lobby.setupLobbyPage();
    this -> setLayout(lobbyLayout);
    this -> adjustSize();
    this -> show();
}

void WindowModule::setUserID (QString id){
    userid = id;
}

QString WindowModule::getUserID (){
    return userid;
}
