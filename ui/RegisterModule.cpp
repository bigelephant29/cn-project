#include "RegisterModule.h"
#include "Constants.h"

#include <QVBoxLayout>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>

QVBoxLayout *RegisterModule :: setupRegisterPage(){
    
    registerLayout = new QVBoxLayout();

    QFormLayout *formLayout = new QFormLayout();
    QHBoxLayout *btnContainer = new QHBoxLayout();

    QLabel *idLabel = new QLabel("New ID:");
    idInput = new QLineEdit();
    idInput -> setMaxLength (MAX_ID_LENGTH);
    QLabel *pwdLabel = new QLabel("Password:");
    pwdInput = new QLineEdit();
    pwdInput -> setMaxLength (MAX_PWD_LENGTH);
    pwdInput -> setEchoMode (QLineEdit::Password);
    QLabel *pwdVerifyLabel = new QLabel("Verify:");
    pwdVerify = new QLineEdit();
    pwdVerify -> setMaxLength (MAX_PWD_LENGTH);
    pwdVerify -> setEchoMode (QLineEdit::Password);

    formLayout -> addRow (idLabel, idInput);
    formLayout -> addRow (pwdLabel, pwdInput);
    formLayout -> addRow (pwdVerifyLabel, pwdVerify);

    regBtn = new QPushButton("Register");
    resetBtn = new QPushButton("Reset");
    cancelBtn = new QPushButton("Cancel");

    QObject::connect(regBtn, SIGNAL(clicked()), this, SLOT(registerOnClick()), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(registerRequest(QString, QString)), this->parent(), SIGNAL(registerRequest(QString, QString)));
    QObject::connect(resetBtn, SIGNAL(clicked()), this, SLOT(resetOnClick()), Qt::UniqueConnection);
    QObject::connect(cancelBtn, SIGNAL(clicked()), this -> parent(), SLOT(switchToMainPage()), Qt::UniqueConnection);

    btnContainer -> addWidget (regBtn);
    btnContainer -> addWidget (resetBtn);
    btnContainer -> addWidget (cancelBtn);

    registerLayout -> addLayout (formLayout);
    registerLayout -> addLayout (btnContainer);
    
    return registerLayout;
    
}

void RegisterModule :: registerOnClick(){
    QString id = idInput -> text();
    QString pwd = pwdInput -> text();
    QString ver = pwdVerify -> text();
    if(id.length() == 0) return;
    pwdInput -> setText (QString(""));
    pwdVerify -> setText (QString(""));
    if(pwd == ver){
        emit registerRequest(id, pwd);
    }   
}

void RegisterModule :: resetOnClick(){
    idInput -> setText (QString(""));
    pwdInput -> setText (QString(""));
    pwdVerify -> setText (QString(""));
}

