#ifndef CHATROOMMODULE
#define CHATROOMMODULE

#include <QGridLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QString>
#include <QListWidget>

class ChatRoomTextFieldModule : public QListWidget{
    Q_OBJECT
public:
public slots:
    void addMyMessage (QString str);
    void addFileTransferMessage (QString msg);
signals:
private:
};


class ChatRoomMsgInModule : public QLineEdit{
    Q_OBJECT
public:
public slots:
    void takeText();
signals:
    void sendLineText (QString);
    void newChatCommand (QString);
private:
};

class ChatRoomModule : public QObject{
    Q_OBJECT
public:
    QGridLayout *setupChatRoom (QString roomName);        /* Function used to set up the chatroom layout */
    QString getRoomName();
private:
    QString roomName;
    ChatRoomTextFieldModule *showText;
public slots:
    void updateMessage(QString,QStringList,quint32,QStringList);
    void updateMyMessage(QString);
    void sendLineText(QString);
    void onFileSelection();
    void fileTransferMessage(QString);
    void newChatCommand (QString);
    void click_download(QListWidgetItem*);
signals:
    void sendText(QString, QString);
    void sendFile(QString);
    void inviteUser(QString, QString);
    void downloadFile(QString,QString);
};



#endif

