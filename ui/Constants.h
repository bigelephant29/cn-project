const int MAX_ID_LENGTH = 12;
const int MAX_PWD_LENGTH = 16;
const int MAX_ROOM_LENGTH = 16;
const int USERLIST_MAX_WIDTH = 128;
const int ROOMLIST_MAX_WIDTH = 128;
const int MESSAGE_MIN_WIDTH = 320;
