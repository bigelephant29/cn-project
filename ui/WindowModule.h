#ifndef WINDOWMODULE
#define WINDOWMODULE

#include "LoginModule.h"
#include "RegisterModule.h"
#include "LobbyModule.h"
#include "Client.h"

#include <QApplication>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QHBoxLayout>

class WindowModule : public QWidget{
    Q_OBJECT
private:
    QString userid;
    LoginModule login;
    RegisterModule regpage;
    LobbyModule lobby;
    QVBoxLayout *loginLayout;
    QVBoxLayout *registerLayout;
    QHBoxLayout *lobbyLayout;
    QGridLayout *chatRoomLayout;
    Client *client;
    void deleteQLayout (QLayout *layout); /* Function used to delete a whole layout including the widgets */

    QLineEdit *ipInput;
    QLineEdit *portInput;
    QFormLayout *connLayout;

public:
    void mainPage();
    void setUserID(QString);
    QString getUserID();

public slots:
    void slot_connect();
    void setupMainPage();
    void setupFailPage();
    void switchToMainPage();        /* switch from register page to main page */
    void switchToRegisterPage();    /* switch from main page to register page */
    void switchToLobbyPage();           /* switch from main page to lobby */

signals:
    void loginSucceed();
    void registerSucceed();
    void loginRequest(QString, QString);
    void registerRequest(QString, QString);
    void getUserListRequest();
    void updateUserList(QString);
    void createRoomRequest(QString);
    void leaveRoomRequest(QString);
    void sendText(QString, QString);
    void sendFile(QString);
    void inviteUser(QString,QString);
    void downloadFile(QString,QString);
    void setRoomName(QString);
    void updateRoomList(QStringList);
};

#endif
