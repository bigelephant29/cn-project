#include "ChatRoomModule.h"
#include "WindowModule.h"
#include "Constants.h"

#include <iostream>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include <QFileDialog>

void ChatRoomModule :: click_download(QListWidgetItem* item) {
    if(!(item->flags() & Qt::ItemIsSelectable)) {
        return;
    }

    QString str = item->text().split(":")[1];
    QString filename = str.mid(9, str.length() - 10);
    QString fileid = item->data(Qt::WhatsThisRole).toString();

    qDebug() << filename;
    qDebug() << fileid;

    QString filepath = QFileDialog::getSaveFileName(NULL, tr("Save File"),
                                                    filename,
                                                    tr("All files (*.*)"));
    if(filepath != "") {
        emit downloadFile(fileid, filepath);
    }
}

QGridLayout *ChatRoomModule :: setupChatRoom(QString roomName){
    /* The chatroom layout */
    QGridLayout *chatRoomLayout = new QGridLayout();

    this -> roomName = roomName;
    QLabel *roomTitle = new QLabel(roomName);
    chatRoomLayout -> addWidget(roomTitle, 0, 0);

    QPushButton *fileTransferBtn = new QPushButton("F");
    fileTransferBtn -> setFixedSize (25, 25);
    QFont btnFont;
    btnFont.setPixelSize (16);
    fileTransferBtn -> setFont(btnFont);
    chatRoomLayout -> addWidget(fileTransferBtn, 1, 0);
    QObject::connect(fileTransferBtn, SIGNAL(clicked()), this, SLOT(onFileSelection()), Qt::UniqueConnection);

    /* show text area */
    showText = new ChatRoomTextFieldModule();
    QObject::connect(showText, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(click_download(QListWidgetItem*)));
    QObject::connect(showText->model(), SIGNAL(rowsInserted(const QModelIndex &, int, int)), showText, SLOT(scrollToBottom()));

    //showText -> setReadOnly(true);
    //showText -> setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextSelectableByKeyboard);
    showText -> setMinimumWidth (MESSAGE_MIN_WIDTH);
    chatRoomLayout -> addWidget(showText, 2, 0, 3, 1);

    /* input box */
    ChatRoomMsgInModule *inputBox = new ChatRoomMsgInModule();
    chatRoomLayout -> addWidget(inputBox, 5, 0);

    /* PushButton for sending message (for TextEdit) */
    QPushButton *sendButton = new QPushButton("Send");
    chatRoomLayout -> addWidget(sendButton, 5, 1);
    
    /* connect send button to show text */
    QObject::connect(sendButton, SIGNAL(clicked()), inputBox, SLOT(takeText()), Qt::UniqueConnection);
    QObject::connect(inputBox, SIGNAL(returnPressed()), inputBox, SLOT(takeText()), Qt::UniqueConnection);
    QObject::connect(inputBox, SIGNAL(sendLineText(QString)), this, SLOT(sendLineText(QString)), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(sendText(QString,QString)), this->parent(), SIGNAL(sendText(QString,QString)), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(sendFile(QString)), this->parent(), SIGNAL(sendFile(QString)), Qt::UniqueConnection);
    QObject::connect(inputBox, SIGNAL(newChatCommand(QString)), this, SLOT(newChatCommand(QString)), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(inviteUser(QString,QString)), this->parent(), SIGNAL(inviteUser(QString,QString)), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(downloadFile(QString,QString)), this->parent(), SIGNAL(downloadFile(QString,QString)), Qt::UniqueConnection);

    return chatRoomLayout;
}

void ChatRoomModule :: updateMessage(QString roomNameCheck, QStringList newMessage, quint32 timeStamp, QStringList fileidlist){
    if(roomNameCheck != roomName) return;
    QStringList :: iterator itr;
    QStringList :: iterator itr_fileid;
    for(itr = newMessage.begin(), itr_fileid = fileidlist.begin() ; itr != newMessage.end() ; itr++, itr_fileid++){

        //showText -> append (*itr);
        if(*itr_fileid == "") {
            QListWidgetItem *item = new QListWidgetItem(*itr);
            item->setFlags(Qt::NoItemFlags | Qt::ItemIsEnabled);
            showText->addItem(item);
        } else {
            QListWidgetItem *item = new QListWidgetItem(*itr);
            item->setFlags(Qt::NoItemFlags | Qt::ItemIsEnabled | Qt::ItemIsSelectable);
            item->setData(Qt::WhatsThisRole, *itr_fileid);
            item->setBackgroundColor(QColor::fromRgb(51, 150, 255));
            showText->addItem(item);
            //showText->addItem(QListWidgetItem());
        }
    }
}

QString ChatRoomModule :: getRoomName(){
    return roomName;
}

void ChatRoomModule :: sendLineText(QString text){
    emit sendText(roomName, text);
}

void ChatRoomModule :: newChatCommand(QString cmd){
    int fs = -1;
    for(int i = 1 ; i < cmd.length() ; i++){
        if(cmd.at(i) == ' '){
            fs = i;
            break;
        }
    }
    if(fs == -1) return;
    QString type = cmd.left(fs);
    QString msg = cmd.right(cmd.length() - (fs+1));
    
    if(type == "/invite" and !roomName.contains("[PM]", Qt::CaseInsensitive)){
        emit inviteUser(roomName, msg);
    }
    if(type == "/download"){
        QStringList list = msg.split(" ", QString::SkipEmptyParts);
        emit downloadFile(list[0], list[1]);
    }
}

void ChatRoomModule :: updateMyMessage(QString str){
    showText -> addMyMessage(str);
}

void ChatRoomModule :: onFileSelection(){
    QString filePath = QFileDialog::getOpenFileName(NULL, tr("Open File"),
                                                    "/home",
                                                    tr("All files (*.*)"));
    if(filePath != "") {
        emit sendFile(filePath);
    }
}

void ChatRoomModule :: fileTransferMessage(QString msg){
    showText -> addFileTransferMessage(msg);
}

void ChatRoomTextFieldModule :: addMyMessage (QString str) {
    QString msg =  ((WindowModule*)(this->parent()))->getUserID() + " : " + str; 
    this -> addItem(msg);
    //this -> append (msg);
}

void ChatRoomTextFieldModule :: addFileTransferMessage (QString msg){
    this -> addItem(msg);
    //this -> append (msg);
}

void ChatRoomMsgInModule :: takeText() {
    if(this -> text() .length() == 0) return;
    if(this -> text() .at(0) == '/'){
        emit newChatCommand( this -> text() );
    }else{
        emit sendLineText( this -> text() );
    }
    this -> clear();
}
