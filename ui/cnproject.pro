TEMPLATE = app
TARGET = cnproject
 
QT = core gui widgets network
 
SOURCES += main.cpp

HEADERS += Constants.h

HEADERS += WindowModule.h
SOURCES += WindowModule.cpp

HEADERS += LoginModule.h
SOURCES += LoginModule.cpp

HEADERS += RegisterModule.h
SOURCES += RegisterModule.cpp

HEADERS += LobbyModule.h
SOURCES += LobbyModule.cpp

HEADERS += ChatRoomModule.h
SOURCES += ChatRoomModule.cpp

HEADERS += Client.h
SOURCES += Client.cpp

