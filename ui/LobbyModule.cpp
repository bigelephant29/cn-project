#include "LobbyModule.h"
#include "ChatRoomModule.h"
#include "Constants.h"
#include "WindowModule.h"

#include <QGridLayout>
#include <QPushButton>
#include <QFont>
#include <QListWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QFormLayout>
#include <QListWidgetItem>
#include <QList>
#include <QDebug>

void LobbyModule :: createPrivateMsg() {
    if(privateMsgNameInput->text().length() == 0) return;
    QString src_user = ((WindowModule*)this->parent())->getUserID();
    QString target_user = privateMsgNameInput->text();

    if(src_user > target_user) {
        src_user = target_user;
        target_user = ((WindowModule*)this->parent())->getUserID();
    }
    privateMsgNameInput->setText("");

    QString room_name = "[PM]" + src_user + "|" + target_user;
    emit createRoomRequest(room_name);
    emit inviteUser(room_name, src_user);
    emit inviteUser(room_name, target_user);

    deleteQLayout(chatRoomLayout);
    chatRoomLayout = chatRoom.setupChatRoom(room_name);
    emit setRoomName(room_name);
    lobbyLayout -> addLayout (chatRoomLayout);
    nowStatus = CHAT;
}
void LobbyModule :: selectPM(QListWidgetItem* item) {
    QString username = item->data(Qt::WhatsThisRole).toString();
    privateMsgNameInput->setText(username);
}

QHBoxLayout *LobbyModule :: setupLobbyPage(){
    
    nowStatus = INIT;

    lobbyLayout = new QHBoxLayout(); 
    userListLayout = new QVBoxLayout();
    roomListLayout = new QVBoxLayout();
    privateMsgLayout = new QFormLayout();
    chatRoomLayout = initializeView();

    QLabel *privateMsgLabel = new QLabel("Private Message :");
    privateMsgLabel -> setFixedHeight(25);
    privateMsgLayout -> addRow(privateMsgLabel);
    privateMsgNameInput = new QLineEdit();
    privateMsgNameInput -> setFixedSize(100, 25);
    QLabel *userNameLabel = new QLabel("User Name :");
    privateMsgLayout -> addRow(userNameLabel);
    privateMsgLayout -> addRow(privateMsgNameInput);
    startPrivateMsgBtn = new QPushButton("Chat");
    startPrivateMsgBtn -> setFixedSize(60,25);
    privateMsgLayout -> addRow(startPrivateMsgBtn);
    QObject::connect(startPrivateMsgBtn, SIGNAL(clicked()), this, SLOT(createPrivateMsg()), Qt::UniqueConnection);

    QLabel *onlineUserLabel = new QLabel("User List :");
    userListLayout -> addWidget(onlineUserLabel);

    refreshBtn = new QPushButton("Refresh");
    refreshBtn -> setFixedHeight(25);
    userListLayout -> addWidget(refreshBtn);
    QObject::connect(refreshBtn, SIGNAL(clicked()), this, SIGNAL(getUserListRequest()));

    userList = new QListWidget();
    userList -> setMaximumWidth (USERLIST_MAX_WIDTH);
    userListLayout -> addWidget(userList);

    QLabel *roomListLabel = new QLabel("Room List :");
    roomListLayout -> addWidget(roomListLabel);

    btnContainer = new QHBoxLayout();
    QFont btnFont;
    btnFont.setPixelSize (16);

    createRoomBtn = new QPushButton("+");
    createRoomBtn -> setFixedSize (25, 25);
    createRoomBtn -> setFont(btnFont);
    btnContainer -> addWidget(createRoomBtn);
    QObject::connect(createRoomBtn, SIGNAL(clicked()), this, SLOT(createRoomPage()), Qt::UniqueConnection);

    leaveRoomBtn = new QPushButton("-");
    leaveRoomBtn -> setFixedSize (25, 25);
    leaveRoomBtn -> setFont(btnFont);
    btnContainer -> addWidget(leaveRoomBtn);
    QObject::connect(leaveRoomBtn, SIGNAL(clicked()), this, SLOT(leaveRoom()), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(leaveRoomRequest(QString)), this->parent(), SIGNAL(leaveRoomRequest(QString)), Qt::UniqueConnection);

    roomListLayout -> addLayout(btnContainer);

    roomList = new QListWidget();
    roomList -> setMaximumWidth (ROOMLIST_MAX_WIDTH);
    roomListLayout -> addWidget(roomList);



    lobbyLayout -> addLayout (userListLayout);
    lobbyLayout -> addLayout (privateMsgLayout);
    lobbyLayout -> addLayout (roomListLayout);
    lobbyLayout -> addLayout (chatRoomLayout);

    chatRoom.setParent(this);
    QObject::connect(this, SIGNAL(sendText(QString,QString)), this->parent(), SIGNAL(sendText(QString,QString)));
    QObject::connect(this, SIGNAL(updateMessage(QString,QStringList,quint32,QStringList)), &chatRoom, SLOT(updateMessage(QString,QStringList,quint32,QStringList)));
    QObject::connect(roomList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(switchRoom(QListWidgetItem*)));
    QObject::connect(this, SIGNAL(getUserListRequest()), this->parent(), SIGNAL(getUserListRequest()));
    QObject::connect(this, SIGNAL(updateMyMessage(QString)), &chatRoom, SLOT(updateMyMessage(QString)));
    QObject::connect(this, SIGNAL(sendFile(QString)), this->parent(), SIGNAL(sendFile(QString)));
    QObject::connect(this, SIGNAL(fileTransferMessage(QString)), &chatRoom, SLOT(fileTransferMessage(QString)));
    QObject::connect(this, SIGNAL(inviteUser(QString,QString)), this->parent(), SIGNAL(inviteUser(QString,QString)));
    QObject::connect(this, SIGNAL(downloadFile(QString,QString)), this->parent(), SIGNAL(downloadFile(QString,QString)));
    QObject::connect(this, SIGNAL(setRoomName(QString)), this->parent(), SIGNAL(setRoomName(QString)));

    QObject::connect(userList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(selectPM(QListWidgetItem*)));

    emit getUserListRequest();

    return lobbyLayout; 
}

QGridLayout *LobbyModule :: initializeView(){
    
    QGridLayout *initView = new QGridLayout();

    QLabel *showText = new QLabel();
    showText -> setText("HI N D is carry\nChat Room System v0.87\n\nTaiwan No.1");
    showText -> setAlignment(Qt::AlignCenter);
    initView -> addWidget(showText, 0, 0, 2, 2);

    return initView;
}

QFormLayout *LobbyModule :: createRoomLayout(){
    QFormLayout *room = new QFormLayout();

    QLabel *roomNameLabel = new QLabel("New Room Name:");
    roomNameInput = new QLineEdit();
    roomNameInput -> setMaxLength (MAX_ID_LENGTH);

    QPushButton *doCreateRoomBtn = new QPushButton("Create");

    room -> addRow(roomNameLabel, roomNameInput);
    room -> addRow(doCreateRoomBtn);

    room -> setFormAlignment(Qt::AlignCenter);

    QObject::connect(doCreateRoomBtn, SIGNAL(clicked()), this, SLOT(createRoom()), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(createRoomRequest(QString)), this->parent(), SIGNAL(createRoomRequest(QString)), Qt::UniqueConnection);

    return room;
}

void LobbyModule :: createRoom (){
    if(roomNameInput->text().length() == 0) return;
    QString newRoomName = roomNameInput->text();
    roomNameInput -> setText(QString(""));
    emit createRoomRequest(newRoomName);
}

void LobbyModule :: addRoom (QString newRoomName){
    roomList -> addItem(newRoomName);
    int itemCount = roomList -> count();
    switchRoom(roomList -> item(itemCount-1));
}

void LobbyModule :: createRoomPage (){
    if(nowStatus != CREATE){
        deleteQLayout(chatRoomLayout);
        chatRoomLayout = createRoomLayout();
        lobbyLayout -> addLayout (chatRoomLayout);
        nowStatus = CREATE;
    }
}

void LobbyModule :: switchRoom(QListWidgetItem *selectedItem){
    deleteQLayout(chatRoomLayout);
    chatRoomLayout = chatRoom.setupChatRoom(selectedItem -> text());
    emit setRoomName(selectedItem -> text());
    lobbyLayout -> addLayout (chatRoomLayout);
    nowStatus = CHAT;
}

void LobbyModule :: deleteQLayout (QLayout *layout){
    QLayoutItem *item;
    while( layout -> count() > 0 ){
        item = layout -> takeAt(0);
        if(item -> layout()){
            deleteQLayout(item -> layout());
        }
        else if(item -> widget()) delete item -> widget();
    }
    delete layout;
}

void LobbyModule :: updateUserList (QStringList newList, QStringList statusList){
    userList -> clear();
    QStringList :: iterator itr;
    QStringList :: iterator itr_status;
    int i;

    for(itr = newList.begin(), itr_status = statusList.begin(), i = 0 ; itr != newList.end() ; itr++, itr_status++, i++){
        if(*itr_status == "offline") {
            userList -> addItem (*itr);
        } else {
            userList -> addItem (*itr + "[online]");
            userList -> item(i) -> setBackgroundColor(QColor::fromRgb(92, 170, 21));
        }
        userList -> item(i) -> setData(Qt::WhatsThisRole, *itr);
    }
}

void LobbyModule :: leaveRoom(){
    QListWidgetItem *nowItem = roomList -> currentItem();
    if(nowItem == NULL) return;
    emit leaveRoomRequest(nowItem -> text());
}

void LobbyModule :: removeRoom(QString leaveRoomName){
    if(leaveRoomName == chatRoom.getRoomName()){
        deleteQLayout(chatRoomLayout);
        chatRoomLayout = initializeView();
        emit setRoomName(QString(""));
        lobbyLayout -> addLayout(chatRoomLayout);
        nowStatus = INIT;
        ((QWidget*)(this -> parent())) -> adjustSize();
    }
    QList<QListWidgetItem*> itemList = roomList -> findItems(leaveRoomName, Qt::MatchExactly);
    if(itemList.count() == 0) return;
    int row = roomList -> row(*itemList.begin());
    QListWidgetItem* nowItem = roomList->takeItem(row);
    delete nowItem;
}

void LobbyModule :: updateRoomList(QStringList nowRoomList){
    QStringList::iterator itr;
    for(int i = 0 ; i < roomList->count() ; i++){
        if(!nowRoomList.removeOne(roomList->item(i)->text())){
            QListWidgetItem *nowItem = roomList->takeItem(i);
            delete nowItem;
            i--;
        }
    }
    for(itr = nowRoomList.begin() ; itr != nowRoomList.end() ; itr++){
        roomList -> addItem(*itr);
    }
}
