#ifndef CLIENT
#define CLIENT


#include <QtCore>
#include <QtNetwork>

class Client : public QThread{
    Q_OBJECT

public:
    explicit Client(QObject *parent, QString ip, QString port);
    bool connectToHost(QString host, quint16 port = 8787);
    void startReceiveRequest();

private:
    QTcpSocket *socket;
    QString userid;
    QString nowRoomName;
    QHash<QString, int> maxTimeStamp;
    bool sendJSON(QJsonObject&);
    bool sendJSON(QJsonDocument&);
    bool readJSON(QJsonDocument&);

protected:
    void run();

signals:
    void connectionSucceed();
    void connectionFail(); 
    void loginSucceed();
    void registerSucceed();
    void updateUserList(QStringList, QStringList);
    void addRoom(QString);
    void removeRoom(QString);
    void updateMessage(QString, QStringList, quint32, QStringList);
    void updateMyMessage(QString);
    void fileTransferMessage(QString);
    void timeout();
    void updateRoomList(QStringList);

public slots:
    void loginRequest(QString,QString);
    void registerRequest(QString,QString);
    void getUserListRequest();
    void createRoomRequest(QString);
    void leaveRoomRequest(QString);
    void sendText(QString, QString);
    void downloadFile(QString, QString);
    void sendFile(QString);
    void inviteUser(QString,QString);
    void updateStatus();
    void setRoomName(QString);
    void slot_uploaded(QString fileid, QString filename);
    void slot_downloaded(QString fileid, QString filepath);
};

#endif
