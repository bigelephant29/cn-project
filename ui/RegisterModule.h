#ifndef REGISTERMODULE
#define REGISTERMODULE

#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>

class RegisterModule : public QObject {
    Q_OBJECT
private:
    QVBoxLayout *registerLayout;
    QLineEdit *idInput;
    QLineEdit *pwdInput;
    QLineEdit *pwdVerify;
    QPushButton *regBtn;
    QPushButton *resetBtn;
    QPushButton *cancelBtn;
public:
    QVBoxLayout *setupRegisterPage();     /* Function used to set up the register page */
public slots:
    void registerOnClick();
    void resetOnClick();
signals:
    void registerSucceed();
    void registerRequest(QString, QString);
};

#endif
