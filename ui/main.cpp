#include <QApplication>
#include "Constants.h"
#include "WindowModule.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    WindowModule *mainWindow = new WindowModule();
    mainWindow -> setWindowTitle ("HI N D is carry");
    mainWindow -> mainPage();
    
    return app.exec();
}
