#ifndef LOBBYMODULE
#define LOBBYMODULE

#include "ChatRoomModule.h"

#include <QGridLayout>
#include <QPushButton>
#include <QListWidget>
#include <QLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QListWidgetItem>

class LobbyModule : public QObject {
    Q_OBJECT
private:
    enum Status {INIT, CREATE, CHAT} nowStatus;
    ChatRoomModule chatRoom;
    QHBoxLayout *lobbyLayout;
    QPushButton *refreshBtn;
    QVBoxLayout *userListLayout;
    QVBoxLayout *roomListLayout;

    QFormLayout *privateMsgLayout;
    QPushButton *startPrivateMsgBtn;
    QLineEdit *privateMsgNameInput;

    QLayout *chatRoomLayout;
    QHBoxLayout *btnContainer;
    QPushButton *createRoomBtn;
    QPushButton *leaveRoomBtn;
    QListWidget *userList;
    QListWidget *roomList;
    QGridLayout *initializeView();
    QFormLayout *createRoomLayout();
    QLineEdit *roomNameInput;
    void deleteQLayout(QLayout *layout);
public:
    QHBoxLayout *setupLobbyPage();         /* Function used to set up Lobby */
public slots:
    void createPrivateMsg();
    void createRoom();
    void createRoomPage();
    void switchRoom(QListWidgetItem*);
    void updateUserList(QStringList, QStringList);
    void addRoom(QString);
    void leaveRoom();
    void removeRoom(QString);
    void updateRoomList(QStringList);
    void selectPM(QListWidgetItem* item);
signals:
    void getUserListRequest();
    void createRoomRequest(QString);
    void leaveRoomRequest(QString);
    void updateMessage(QString,QStringList,quint32,QStringList);
    void sendText(QString,QString);
    void updateMyMessage(QString);
    void sendFile(QString);
    void fileTransferMessage(QString);
    void inviteUser(QString,QString);
    void downloadFile(QString,QString);
    void setRoomName(QString);
};


#endif
