#include "LoginModule.h"
#include "WindowModule.h"
#include "Constants.h"

#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QObject>
#include <QApplication>

QVBoxLayout *LoginModule :: setupLoginPage(){

    loginLayout = new QVBoxLayout();

    QFormLayout *formLayout = new QFormLayout();
    QHBoxLayout *btnContainer = new QHBoxLayout();

    QLabel *idLabel = new QLabel("ID:");
    idInput = new QLineEdit();
    idInput -> setMaxLength (MAX_ID_LENGTH);
    QLabel *pwdLabel = new QLabel("Password:");
    pwdInput = new QLineEdit();
    pwdInput -> setMaxLength (MAX_PWD_LENGTH);
    pwdInput -> setEchoMode (QLineEdit::Password);

    formLayout -> addRow (idLabel, idInput);
    formLayout -> addRow (pwdLabel, pwdInput);

    loginBtn = new QPushButton("Login");
    regBtn = new QPushButton("Register");
    cancelBtn = new QPushButton("Cancel");

    QObject::connect(loginBtn, SIGNAL(clicked()), this, SLOT(checkLoginOnClick()), Qt::UniqueConnection);
    QObject::connect(this, SIGNAL(loginRequest(QString,QString)), this->parent(), SIGNAL(loginRequest(QString,QString)),Qt::UniqueConnection);
    QObject::connect(regBtn, SIGNAL(clicked()), this->parent(), SLOT(switchToRegisterPage()), Qt::UniqueConnection);
    QObject::connect(cancelBtn, SIGNAL(clicked()), QApplication::instance(), SLOT(quit()), Qt::UniqueConnection);

    btnContainer -> addWidget (loginBtn);
    btnContainer -> addWidget (regBtn);
    btnContainer -> addWidget (cancelBtn);

    loginLayout -> addLayout (formLayout);
    loginLayout -> addLayout (btnContainer);
    
    return loginLayout;
}

void LoginModule :: checkLoginOnClick(){
    QString id = idInput -> text();
    QString pwd = pwdInput -> text();
    if(id.length() == 0) return;
    pwdInput -> setText (QString(""));
    ((WindowModule*)(this -> parent())) -> setUserID(id);
    emit loginRequest(id, pwd);
}

