#include "Client.h"

#include <QtNetwork>
#include <QtCore>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QThread>

QString SERVER_IP;
int SERVER_PORT;

class UploadFile : public QThread {
    Q_OBJECT
    private:
    QTcpSocket *conn;
    QString filepath;
    QString fileid;
    QString filename;

    signals:
    void uploaded(QString fileid, QString filename);

    public:
    UploadFile(QString _filepath, QString _fileid, QString _filename) : filepath(_filepath), fileid(_fileid), filename(_filename) {}
    void run() {
        conn = new QTcpSocket();
        conn->connectToHost(SERVER_IP, SERVER_PORT);
        conn->waitForConnected(-1);

        QJsonObject obj;
        obj["type"] = "upload_file";
        obj["fileid"] = fileid;
        sendJSON(obj);

        upload_file(filepath);

        emit uploaded(fileid, filename);
        quit();
    }
    bool sendJSON(QJsonObject &obj) {
        QJsonDocument doc;
        doc.setObject(obj);

        QByteArray jsonbuf;
        QByteArray data;
        QDataStream datastream(&data, QIODevice::ReadWrite);
        datastream.setVersion(QDataStream::Qt_5_0);

        jsonbuf = doc.toBinaryData();
        datastream << (quint64)jsonbuf.size();
        datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
        conn->write(data);

        return conn->waitForBytesWritten(-1);
    }
    bool upload_file(QString path) {
        QFile file(path);

        if(!file.open(QIODevice::ReadOnly)) {
        return false;
        }

        QDataStream netstream(conn);
        char buf[65536];
        
        netstream << (quint64)file.size();

        while(true) {
        qint64 ret = file.read(buf, sizeof(buf));
        if(ret <= 0) {
            break;
        }
        netstream.writeRawData(buf, ret);
        conn->flush();
        conn->waitForBytesWritten(-1);
        }
        file.close();
        conn->flush();
        conn->waitForBytesWritten(-1);
        conn->waitForDisconnected();
        return true;
    }
};
class DownloadFile : public QThread {
    Q_OBJECT
    private:
    QTcpSocket *conn;
    QString filepath;
    QString fileid;

    signals:
    void downloaded(QString fileid, QString filepath);

    public:
    DownloadFile(QString _filepath, QString _fileid) : filepath(_filepath), fileid(_fileid) {}
    void run() {
        conn = new QTcpSocket();
        conn->connectToHost(SERVER_IP, SERVER_PORT);
        conn->waitForConnected(-1);

        QJsonObject obj;
        obj["type"] = "download_file";
        obj["fileid"] = fileid;
        sendJSON(obj);

        download_file(filepath);

        emit downloaded(fileid, filepath);
        quit();
    }
    bool sendJSON(QJsonObject &obj) {
        QJsonDocument doc;
        doc.setObject(obj);

        QByteArray jsonbuf;
        QByteArray data;
        QDataStream datastream(&data, QIODevice::ReadWrite);
        datastream.setVersion(QDataStream::Qt_5_0);

        jsonbuf = doc.toBinaryData();
        datastream << (quint64)jsonbuf.size();
        datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
        conn->write(data);
        conn->flush();
        return conn->waitForBytesWritten(-1);
    }
    bool read_buffer(QDataStream &outstream) {
        QDataStream datastream(conn);
        datastream.setVersion(QDataStream::Qt_5_0);
        quint64 remain;
        quint64 length;

        if(conn->bytesAvailable() == 0 && !conn->waitForReadyRead(-1)) {
        return false;
        }
        remain = conn->bytesAvailable();
        Q_ASSERT(remain >= 8);
        datastream >> length;
        remain -= 8;

        qDebug() << length;

        while(length > 0) {
        if(remain > 0) {
            char *buf = new char[remain];
            int len = datastream.readRawData(buf, remain);
            if(len == -1) {
            delete buf;
            return false;
            } else {
            outstream.writeRawData(buf, len);
            length -= len;
            delete buf;
            }
        }
        if(length <= 0) {
            break;
        }

        if(conn->bytesAvailable() == 0 && !conn->waitForReadyRead(-1)) {
            return false;
        }
        remain = conn->bytesAvailable();
        }

        return true;
    }
    bool download_file(QString path) {
        QFile file(path);

        if(!file.open(QIODevice::WriteOnly)) {
        return false;
        }

        QDataStream datastream(&file);
        read_buffer(datastream);
        file.flush();
        file.close();

        conn->waitForDisconnected();
        return true;
    }
};

Client :: Client (QObject *parent, QString ip, QString port) : QThread(parent){
    SERVER_IP = ip;
    SERVER_PORT = port.toInt();
}

bool Client :: connectToHost (QString host, quint16 port){
    socket -> connectToHost (host, port);
    return socket -> waitForConnected(20000); // wait for 20 secs
}

void Client :: run() {

    socket = new QTcpSocket();
    socket -> moveToThread(this);

    QObject::connect(this, SIGNAL(connectionSucceed()), this->parent(), SLOT(setupMainPage()));
    QObject::connect(this, SIGNAL(connectionFail()), this->parent(), SLOT(setupFailPage()));
    QObject::connect(this, SIGNAL(loginSucceed()), this->parent(), SIGNAL(loginSucceed()));
    QObject::connect(this, SIGNAL(registerSucceed()), this->parent(), SIGNAL(registerSucceed()));

    if( connectToHost(SERVER_IP, SERVER_PORT) ){
        emit connectionSucceed();
    }else{
        emit connectionFail();
    }

}

bool Client :: sendJSON(QJsonObject &obj) {
    QJsonDocument doc;
    doc.setObject(obj);
    return sendJSON(doc);
}

bool Client :: sendJSON(QJsonDocument &doc) {
    QByteArray jsonbuf;
    QByteArray data;
    QDataStream datastream(&data, QIODevice::ReadWrite);
    datastream.setVersion(QDataStream::Qt_5_0);

    jsonbuf = doc.toBinaryData();
    datastream << (quint64)jsonbuf.size();
    datastream.writeRawData(jsonbuf.constData(), jsonbuf.size());
    socket->write(data);
    socket->flush();
    socket->waitForBytesWritten(-1);
    return true;
}

bool Client :: readJSON(QJsonDocument &doc) {
    QDataStream datastream(socket);
    datastream.setVersion(QDataStream::Qt_5_0);
    QByteArray jsonbuf;
    quint64 remain;
    quint64 length;

    if(socket->bytesAvailable() == 0 && !socket->waitForReadyRead(-1)) {
    return false;
    }

    remain = socket->bytesAvailable();
    Q_ASSERT(remain >= 8);
    datastream >> length;
    remain -= 8;

    while(length > 0) {
    if(remain > 0) {
        char *buf = new char[std::min(length, remain)];
        datastream.readRawData(buf, std::min(length, remain));
        jsonbuf.append(buf, std::min(length, remain));
        length -= std::min(length, remain);
        delete buf;
    }
    if(length == 0) {
        break;
    }
    if(socket->bytesAvailable() == 0 && !socket->waitForReadyRead(-1)) {
        return false;
    }
    remain = socket->bytesAvailable();
    }

    doc = QJsonDocument::fromBinaryData(jsonbuf);
    return true;
}

void Client::updateStatus(){
    int i;
    QStringList roomList;
    QJsonObject obj;
    QJsonDocument doc;
    QJsonArray jsonlist;
    QStringList strList;

    obj["type"] = "get_roomlist";

    sendJSON(obj);
    readJSON(doc);

    jsonlist = doc.object()["list"].toArray();
    for(i = 0;i < jsonlist.size();i++) {
        roomList << jsonlist[i].toString();
        if (maxTimeStamp.find(jsonlist[i].toString()) == maxTimeStamp.end()){
            maxTimeStamp[jsonlist[i].toString()] = 0;
        }
    }

    if(roomList.count() > 0){
        emit updateRoomList(roomList);
    }
    if(nowRoomName.length() != 0){
        QStringList newMessage;
        QStringList fileidlist;
        quint32 timeStamp = maxTimeStamp[nowRoomName];

        obj["type"] = "get_message";
        obj["room"] = nowRoomName;
        obj["timestamp"] = (int)timeStamp;
    
        sendJSON(obj);
        readJSON(doc);

        jsonlist = doc.object()["list"].toArray();
        for(i = 0;i < jsonlist.size();i++) {
            QJsonObject msgobj = jsonlist[i].toObject();
            newMessage << msgobj["sender"].toString() + " : " + msgobj["text"].toString();        
            timeStamp = qMax(timeStamp, (quint32)msgobj["timestamp"].toInt());
            fileidlist << msgobj["fileid"].toString();
        }

        if(newMessage.count() > 0){
            maxTimeStamp[nowRoomName] = timeStamp + 1;
            emit updateMessage(nowRoomName, newMessage, timeStamp, fileidlist);
        }
    }
}

void Client :: loginRequest(QString id, QString pwd) {
    QJsonObject obj;
    QJsonDocument doc;

    obj["type"] = "login";
    obj["account"] = id;
    obj["password"] = pwd;

    sendJSON(obj);
    readJSON(doc);
    if (doc.object()["status"] == "ok") {
        userid = id;
        emit loginSucceed();
    }
}

void Client :: registerRequest(QString id, QString pwd) {
    QJsonObject obj;
    QJsonDocument doc;

    obj["type"] = "register";
    obj["account"] = id;
    obj["password"] = pwd;

    sendJSON(obj);
    readJSON(doc);
    if (doc.object()["status"] == "ok")
        emit registerSucceed();
}

void Client :: getUserListRequest(){
    int i;
    QJsonObject obj;
    QJsonDocument doc;
    QJsonArray jsonlist;
    QStringList strList;
    QStringList statusList;

    obj["type"] = "get_userlist";

    sendJSON(obj);
    readJSON(doc);

    jsonlist = doc.object()["list"].toArray();
    for(i = 0;i < jsonlist.size();i++) {
        strList << jsonlist[i].toObject()["account"].toString();
        statusList << jsonlist[i].toObject()["status"].toString();
    }
    emit updateUserList(strList, statusList);
}

void Client :: createRoomRequest(QString newRoomName){
    QJsonObject obj;
    QJsonDocument doc;
    
    obj["type"] = "join_room";
    obj["room"] = newRoomName;
    obj["account"] = userid;
    
    sendJSON(obj);
    readJSON(doc);
    if (doc.object()["status"] == "ok"){
        maxTimeStamp[newRoomName] = 0;
        emit addRoom(newRoomName);
    }
}

void Client :: leaveRoomRequest(QString leaveRoomName){
    QJsonObject obj;
    QJsonDocument doc;

    obj["type"] = "leave_room";
    obj["room"] = leaveRoomName;

    sendJSON(obj);
    readJSON(doc);

    if (doc.object()["status"] == "ok"){
        maxTimeStamp.remove(leaveRoomName);
        emit removeRoom(leaveRoomName);
    }
}

void Client :: sendText(QString roomName, QString msg){
    QJsonObject obj;
    QJsonDocument doc;

    obj["type"] = "send_message";
    obj["room"] = roomName;
    obj["text"] = msg;
    
    sendJSON(obj);
    readJSON(doc);
    /* doesn't need to emit updateMyMessage(msg), since updateStatus will update the text */
}

void Client :: setRoomName(QString roomName){
    if (nowRoomName != roomName)
        maxTimeStamp[nowRoomName] = 0;
    nowRoomName = roomName;
}

void Client :: slot_uploaded(QString fileid, QString filename){
    QJsonObject obj;
    QJsonDocument doc;

    obj["type"] = "send_message";
    obj["room"] = nowRoomName;
    obj["text"] = QString("[upload " + filename + "]");
    obj["fileid"] = fileid;
    
    sendJSON(obj);
    readJSON(doc);

    return;
}

void Client :: sendFile(QString filepath){
    /* TODO : send file request json to server */
    // emit sendFileMessage(QString) on success, do nothing on fail
    QJsonObject obj;
    QJsonDocument doc;

    QFile file(filepath);
    QFileInfo fileinfo(file);

    obj["type"] = "create_file";
    obj["filename"] = fileinfo.fileName();
    sendJSON(obj);
    readJSON(doc);
    QString fileid = doc.object()["fileid"].toString();

    UploadFile *filethread = new UploadFile(filepath, fileid, fileinfo.fileName());
    filethread->start();
    connect(filethread, SIGNAL(uploaded(QString, QString)), this, SLOT(slot_uploaded(QString, QString)));
    connect(filethread, SIGNAL(finished()), filethread, SLOT(deleteLater()));

    emit fileTransferMessage("Client is sending " + filepath + " to server.");
}

void Client :: slot_downloaded(QString fileid, QString filepath){
    QJsonObject obj;
    QJsonDocument doc;

    obj["type"] = "send_message";
    obj["room"] = nowRoomName;
    obj["text"] = QString("[downloaded " + filepath + "]");
    
    sendJSON(obj);
    readJSON(doc);

    return;
}

void Client :: downloadFile(QString fileid, QString filePath) {
    DownloadFile *filethread = new DownloadFile(filePath, fileid);
    filethread->start();
    connect(filethread, SIGNAL(downloaded(QString, QString)), this, SLOT(slot_downloaded(QString, QString)));
    connect(filethread, SIGNAL(finished()), filethread, SLOT(deleteLater()));
}

void Client :: inviteUser(QString roomName, QString userName){
    QJsonObject obj;
    QJsonDocument doc;
    
    obj["type"] = "join_room";
    obj["room"] = roomName;
    obj["account"] = userName;
    
    sendJSON(obj);
    readJSON(doc);
}

#include "Client.moc"
